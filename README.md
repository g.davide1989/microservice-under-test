# Microservice Under Test

## Description

Skeleton microservice under test, used to prove the CI/CD pipeline.

## CI/CD

The pipeline has the following steps:

- Compile
- Static code analizers (SonarQube)
- Acceptance tests at component level
- Deploy

